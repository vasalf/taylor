#!/usr/bin/python3

from taylor import *

def Sh(p, n):
    return (Exp(p, n) - Exp(-1 * p, n)) // 2


x = FromPoly(Poly([0, 1]))
print(Ln(x, 2) - x)
print(Exp(x, 2) - 1 - x)
print(Cos(x, 4) - 1 + x ** 2 // 2)
print()


