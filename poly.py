#!/usr/bin/python3

from fractions import Fraction

class Poly:
    def __reduce(self):
        i = len(self) - 1
        while i > 0 and self.__coef[i] == 0:
            i -= 1
        self.__coef = self.__coef[:i + 1]

    def __init__(self, arg=None, should_copy=True):
        self.__symbol_string = "".join(map(chr, [8304, 185, 178, 179, 8308, 8309, 8310, 8311, 8312, 8313]))
        if arg is None:
            self.__coef = [0]
        elif type(arg) in [int, float, Fraction]:
            self.__coef = [arg]
        elif type(arg) == list:
            if should_copy:
                self.__coef = arg[:]
            else:
                self.__coef = arg
        elif type(arg) == tuple:
            self.__coef = list(arg)
        elif type(arg) == str:
            self.__coef = list(map(eval, arg.split()))
        elif type(arg) == Poly:
            self.__coef = arg.__coef[:]
        self.__reduce()
        
    def __int2upper_str(self, n):
        return "".join(map(lambda c: self.__symbol_string[ord(c) - ord('0')], str(n)))

    def __monomial2str(self, i):
        res = ""

        if i == 0:
            if type(self.__coef[i]) == float:
                if self.__coef[i] < 0:
                    if 1 == len(self.__coef):
                        return str(round(self.__coef[i], 3))
                    else:
                        return "- " + str(-round(self.__coef[i], 3))
                else:
                    if 1 == len(self.__coef):
                        return str(round(self.__coef[i], 3))
                    else:
                        return "+ " + str(round(self.__coef[i], 3))
            elif type(self.__coef[i]) == Fraction and self.__coef[i].denominator != 1:
                if self.__coef[i] < 0:
                    if 1 == len(self.__coef):
                        return "-(" + str(-self.__coef[i]) + ")"
                    else:
                        return "- (" + str(-self.__coef[i]) + ")"
                else:
                    if 1 == len(self.__coef):
                        return "(" + str(self.__coef[i]) + ")"
                    else:
                        return "+ (" + str(self.__coef[i]) + ")"
            else:
                if 1 == len(self.__coef):
                    return str(self.__coef[i])
                else:
                    if self.__coef[i] < 0:
                        return "- " + str(-self.__coef[i])
                    else:
                        return "+ " + str(self.__coef[i])

        if self.__coef[i] < 0:
            res += "-"
        elif i < len(self.__coef) - 1:
            res += "+"
        if i < len(self.__coef) - 1:
            res += " "

        if type(self.__coef[i]) == Fraction:
            if self.__coef[i].denominator != 1:
                res += "(" + str(abs(self.__coef[i])) + ")"
            elif self.__coef[i].denominator == 1:
                if abs(self.__coef[i].numerator) != 1:
                    res += str(abs(self.__coef[i].numerator))
        elif type(self.__coef[i]) == int:
            if abs(self.__coef[i]) != 1:
                res += str(abs(self.__coef[i]))
        else:
            k = abs(round(self.__coef[i], 3))
            if k != 1:
                res += str(k)

        if i == 0:
            return res
        elif i == 1:
            res += "x"
        else:
            res += "x" + self.__int2upper_str(i)
        return res
            
    def __str__(self):
        if len(self.__coef) == 1 and (self.__coef[0] == 0 or self.__coef[0] == Fraction(0)):
            return "0"
        res = []
        for i in range(len(self.__coef) - 1, -1, -1):
            if self.__coef[i] != 0:
                res.append(self.__monomial2str(i))
        return " ".join(res)

    def __len__(self):
        return len(self.__coef)

    def __add__(self, other):
        if type(other) == Poly:
            a = len(self)
            b = len(other)
            l = max(a, b)
            res = [0 for i in range(l)]
            for i in range(l):
                if i < a:
                    res[i] += self.__coef[i]
                if i < b:
                    res[i] += other.__coef[i]
            return Poly(res, False)
        else:
            res = self.__coef[:]
            res[0] = res[0] + other
            return Poly(res)

    def __radd__(self, other):
        return self + other

    def __iadd__(self, other):
        self.__coef = (self + other).__coef
        return self

    def __sub__(self, other):
        if type(other) == Poly:
            a = len(self)
            b = len(other)
            l = max(a, b)
            res = [0 for i in range(l)]
            for i in range(l):
                if i < a:
                    res[i] += self.__coef[i]
                if i < b:
                    res[i] -= other.__coef[i]
            return Poly(res, False)
        else:
            res = self.__coef[:]
            res[0] = res[0] - other
            return Poly(res, False)

    def __rsub__(self, other):
        res = self.__coef[:]
        for i in range(len(res)):
            res[i] = -res[i]
        return other + Poly(res)

    def __isub__(self, other):
        self.__coef = (self - other).__coef
        return self

    def __mul__(self, other):
        if type(other) == Poly:
            res = [0 for i in range(len(self) + len(other))]
            for i in range(len(self)):
                for j in range(len(other)):
                    res[i + j] += other.__coef[j] * self.__coef[i]
            return Poly(res, False)
        else:
            res = self.__coef[:]
            for i in range(len(res)):
                res[i] = other * res[i]
            return Poly(res, False)

    def __rmul__(self, other):
        return self * other

    def __imul__(self, other):
        self.__coef = (self * other).__coef
        return self

    def __pow__(self, other):
        if other == 0:
            return Poly(1)
        elif other & 1:
            return self * (self ** (other - 1))
        else:
            t = self ** (other // 2)
            return t * t

    def __ipow__(self, other):
        self.__coef = (self ** other).__coef
        return self

    def __or__(self, other):
        ans = 0
        for i in range(len(self) - 1, -1, -1):
            ans = ans * other + self.__coef[i]
        return ans

    def __divmod__(self, other):
        if type(other) == Poly:
            q = self.__coef[:]
            b = [0 for i in range(len(self) - len(other) + 1)]
            if b == []:
                b = [0]
            i = len(q) - 1
            while i + 1 >= len(other):
                monom = Fraction(q[i]) / Fraction(other.__coef[-1])
                for j in range(len(other)):
                    q[j + i - len(other) + 1] -= monom * other.__coef[j]
                b[i - len(other) + 1] += monom
                i -= 1
            return Poly(b), Poly(q)
        else:
            return divmod(self, Poly(other))

    def __rdivmod__(self, other):
        return divmod(Poly(other), self)

    def __floordiv__(self, other):
        return divmod(self, other)[0]
    
    def __rfloordiv__(self, other):
        return divmod(Poly(other), self)[0]
        
    def __ifloordiv__(self, other):
        self.__coef = (self // other).__coef
        return self
       
    def __mod__(self, other):
        return divmod(self, other)[1]
    
    def __rmod__(self, other):
        return divmod(Poly(other), self)[1]

    def __imod__(self, other):
        self.__coef = (self % other).__coef
        return self

    def __getitem__(self, i):
        if i >= len(self):
            raise IndexError()
        elif i < 0:
            raise IndexError()
        return self.__coef[i]

    def __setitem__(self, i, val):
        if i < 0:
            raise IndexError()
        elif i >= len(self):
            self.__coef += [0] * (i - len(self) + 1)
        self.__coef[i] = val

    def __call__(self, q):
        if type(q) != Poly:
            return self | q
        ans = Poly()
        for i in range(len(self) - 1, -1, -1):
            ans = ans * q + self.__coef[i]
        return ans


