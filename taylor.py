#!/usr/bin/python3

from fractions import Fraction
from poly import Poly

class TaylorSeries:
    """
        self.p is a Poly in the series
        self.o is n from o(X^n) summand
    """
    def __init__(self, p, o):
        self.p = Poly(p)
        self.o = o
        i = self.o + 1
        while len(self.p) > i:
            if self.p[i] != 0:
                self.p -= self.p[i] * (Poly([0, 1]) ** i)
            i += 1

    def __add__(self, other):
        if isinstance(other, TaylorSeries):
            return TaylorSeries(self.p + other.p, min(self.o, other.o))
        else:
            return TaylorSeries(self.p + other, self.o)

    def __radd__(self, other):
        return self + other

    def __iadd__(self, other):
        self = self + other
        return self

    def __sub__(self, other):
        if isinstance(other, TaylorSeries):
            return TaylorSeries(self.p - other.p, min(self.o, other.o))
        else:
            return TaylorSeries(self.p - other, self.o)

    def __rsub__(self, other):
        if isinstance(other, TaylorSeries):
            return other - self
        else:
            return TaylorSeries(other - self.p, self.o)

    def __isub__(self, other):
        self = self - other
        return self

    def __mul__(self, other):
        if isinstance(other, TaylorSeries):
            return TaylorSeries(self.p * other.p, self.o + other.o)
        else:
            return TaylorSeries(self.p * other, self.o)

    def __rmul__(self, other):
        return self * other

    def __imul__(self, other):
        self = self * other
        return self

    def __pow__(self, other):
        assert type(other) == int
        assert other >= 0
        if other == 0:
            return TaylorSeries(1, 0)
        elif other & 1:
            return self * (self ** (other ^ 1))
        else:
            t = self ** (other >> 1)
            return t * t

    def __ipow__(self, other):
        self = self ** other
        return self

    def __floordiv__(self, other):
        assert not isinstance(other, TaylorSeries)
        return TaylorSeries(self.p // other, self.o)

    def __ifloordiv__(self, other):
        self = self // other
        return self

    def __str__(self):
        return "o(" + str(Poly([0, 1]) ** self.o) + ") + (" + str(self.p) + ")"


def Exp(p, n):
    ans = Fraction(1)
    fct = 1
    for i in range(1, n + 1):
        fct *= i
        ans += (p ** i) // fct
    x = 0
    while p.p[x] == 0:
        x += 1
    ans += TaylorSeries(Poly(0), x * n)
    return ans


def Sin(p, n):
    if n % 2 == 1:
        n += 1
    ans = Fraction(0)
    fct = 1
    for i in range(1, n + 1):
        fct *= i
        if i % 2 == 1:
            if i % 4 == 3:
                ans -= (p ** i) // fct
            else:
                ans += (p ** i) // fct
    x = 0
    while p.p[x] == 0:
        x += 1
    ans += TaylorSeries(Poly(0), x * n)
    return ans


def Cos(p, n):
    if n % 2 == 0:
        n += 1
    ans = Fraction(1)
    fct = 1
    for i in range(1, n + 1):
        fct *= i
        if i % 2 == 0:
            if i % 4 == 2:
                ans -= (p ** i) // fct
            else:
                ans += (p ** i) // fct
    x = 0
    while p.p[x] == 0:
        x += 1
    ans += TaylorSeries(Poly(0), x * n)
    return ans

    
# ln(1 + p)
def Ln(p, n):
    ans = Fraction(0)
    for i in range(1, n + 1):
        if i % 2 == 0:
            ans -= (p ** i) // i
        else:
            ans += (p ** i) // i
    x = 0
    while p.p[x] == 0:
        x += 1
    ans += TaylorSeries(Poly(0), x * n)
    return ans


# (1 + p)^a
def AlphaPow(p, a, n):
    ans = Fraction(1)
    cur = 1
    fct = 1
    for i in range(1, n + 1):
        fct *= i
        cur *= (a - (i - 1))
        ans += cur * (p ** i) // fct
    x = 0
    while p.p[x] == 0:
        x += 1
    ans += TaylorSeries(Poly(0), x * n)
    return ans


def Arcsin(p, n):
    ans = Fraction(0)
    cur = 1
    for i in range(1, n + 1):
        if i % 2 == 0:
            cur *= Fraction(i - 1, i)
        else:
            ans += cur * (p ** i) // i
    x = 0
    while p.p[x] == 0:
        x += 1
    ans += TaylorSeries(Poly(0), x * n)
    return ans


def Arctan(p, n):
    ans = Fraction(0)
    for i in range(1, n + 1):
        if i % 2 == 1:
            if i % 4 == 1:
                ans += (p ** i) // i
            else:
                ans -= (p ** i) // i
    x = 0
    while p.p[x] == 0:
        x += 1
    ans += TaylorSeries(Poly(0), x * n)
    return ans


def FromPoly(p):
    return TaylorSeries(p, 100500)
